<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/', function (Request $request, Response $response, $args) {
    // Render index view
    return $this->view->render($response, 'index.latte');
})->setName('index');

$app->post('/test', function (Request $request, Response $response, $args) {
    //read POST data
    $input = $request->getParsedBody();

    //log
    $this->logger->info('Your name: ' . $input['person']);

    return $response->withHeader('Location', $this->router->pathFor('index'));
})->setName('redir');

//PERSONS
//_________________________________________________________________________________________
//_________________________________________________________________________________________
//_________________________________________________________________________________________
// Vypis tabulky osob
$app->get('/persons', function (Request $request, Response $response, $args) {
    $stmt['persons'] = $this->db->query("SELECT * FROM person ORDER BY first_name")->fetchAll();

    return $this->view->render($response, 'persons.latte', $stmt);
})->setName('persons');


//_________________________________________________________________________________________
//Info jednotlive osoby
$app->get('/persons/info', function (Request $request, Response $response, $args) {
    //Vypis tabulky info a adresy
    $idPerson = $request->getQueryParams()['id_person'];
    $stmt = $this->db->prepare("SELECT * FROM person LEFT JOIN location USING (id_location) WHERE id_person = :id_person");
    $stmt->bindValue(":id_person", empty($idPerson) ? null : $idPerson);
    $stmt->execute();
    $tplVars['info_adress'] = $stmt->fetch();

    if (empty($tplVars['info_adress'])) {
        exit('Person not found');
    }

    //Vypis tabulky meetingu
    $stmt = $this->db->prepare("SELECT * FROM person LEFT JOIN person_meeting USING (id_person) LEFT JOIN meeting USING (id_meeting) 
                                WHERE id_person = :id_person ORDER BY start");
    $stmt->bindValue(':id_person', empty($idPerson) ? null : $idPerson);
    $stmt->execute();
    $tplVars['meeting'] = $stmt->fetchAll();

    //Vypis kontaktu
    $stmt = $this->db->prepare("SELECT * FROM person LEFT JOIN contact USING (id_person) LEFT JOIN contact_type USING (id_contact_type) 
                                WHERE id_person = :id_person ORDER BY contact");
    $stmt->bindValue(':id_person', empty($idPerson) ? null : $idPerson);
    $stmt->execute();
    $tplVars['contact'] = $stmt->fetchAll();

    //Vypis vztahu
    $stmt = $this->db->prepare("(SELECT id_relation, id_person1 as id_person, description, name as relation_type, first_name, last_name FROM relation
    LEFT JOIN relation_type USING (id_relation_type) 
    LEFT JOIN (SELECT id_person as id_person1, first_name, last_name FROM person) as person1 USING (id_person1)
    WHERE id_person2 = :id_person)
    UNION
    (SELECT id_relation, id_person2 as id_person, description, name as relation_type, first_name, last_name FROM relation
    LEFT JOIN relation_type USING (id_relation_type) 
    LEFT JOIN (SELECT id_person as id_person2, first_name, last_name FROM person) as person2 USING (id_person2)
    WHERE id_person1 = :id_person)
    ORDER BY id_relation");
    $stmt->bindValue(':id_person', empty($idPerson) ? null : $idPerson);
    $stmt->execute();
    $tplVars['relation'] = $stmt->fetchAll();

    return $this->view->render($response, 'persons_info.latte', $tplVars);
})->setName('persons_info');


//_________________________________________________________________________________________
// Fromular pro pridani nove osoby
$app->get('/persons/new', function (Request $request, Response $response, $args) {
    $tplVars['formData'] = [
        'first_name' => '',
        'last_name' => '',
        'nickname' => '',
        'id_location' => null,
        'gender' => '',
        'height' => '',
        'birth_day' => '',
        'city' => '',
        'street_name' => '',
        'street_number' => '',
        'zip' => '',
    ];

    return $this->view->render($response, 'person_new.latte', $tplVars);
})->setName('person_new');

function insert_location($db, $formData){
    try{
        $stmt = $db->prepare("INSERT INTO location (city, street_name, street_number, zip) VALUES 
                                (:city, :street_name, :street_number, :zip)");
        $stmt->bindValue(':city', empty($formData['city']) ? null : $formData['city']);
        $stmt->bindValue(':street_number', empty($formData['street_number']) ? null : $formData['street_number']);
        $stmt->bindValue(':street_name', empty($formData['street_name']) ? null : $formData['street_name']);
        $stmt->bindValue(':zip', empty($formData['zip']) ? null : $formData['zip']);
        $stmt->execute();
        return $db->lastInsertId('location_id_location_seq');

    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

//_________________________________________________________________________________________
// Odeslani formulare pro pridani osoby
$app->post('/persons/new', function (Request $request, Response $response, $args) {
    $formData = $request->getParsedBody();
    if (empty($formData['first_name']) || empty($formData['last_name']) || empty($formData['nickname'])) {
        $tplVars['message'] = 'Fill required fields';
    } else {
        $id_location = null;
        if (!empty($formData['city']) || !empty($formData['street_name']) || !empty($formData['street_number']) || !empty($formData['zip'])){
            $id_location = insert_location($this->db, $formData);
        }

            try {
            $stmt = $this->db->prepare("INSERT INTO person (nickname, first_name, last_name, id_location, birth_day, height, gender)
                                        VALUES (:nickname, :first_name, :last_name, :id_location, :birth_day, :height, :gender)");
            $stmt->bindValue(':nickname', $formData['nickname']);
            $stmt->bindValue(':first_name', $formData['first_name']);
            $stmt->bindValue(':last_name', $formData['last_name']);
            $stmt->bindValue(':id_location', $id_location );
            $stmt->bindValue(':gender', empty($formData['gender']) ? null : $formData['gender'] );
            $stmt->bindValue(':height', empty($formData['height']) ? null : $formData['height']);
            $stmt->bindValue(':birth_day', empty($formData['birth_day']) ? null : $formData['birth_day']);

            $stmt->execute();
        } catch (PDOException $e){
            $tplVars['message'] = 'Sorry, error';
            $this->logger->error($e->getMessage());
        }
    }
    $tplVars['formData'] = $formData;
    return $response->withHeader('Location', $this->router->pathFor('persons'));
});


//_________________________________________________________________________________________
// Edit profilu
$app->get('/persons/edit', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    if (empty($params['id_person'])) {
        exit('id_person is missing');
    } else {
        $stmt = $this->db->prepare("SELECT * FROM person LEFT JOIN location ON (person.id_location = location.id_location)
                                        WHERE id_person = :id_person");
        $stmt->bindValue(':id_person', $params['id_person']);
        $stmt->execute();
        $tplVars['formData'] = $stmt->fetch();
        if (empty($tplVars['formData'])) {
            exit('Person not found');
        }
    }

        return $this->view->render($response, 'person_edit.latte', $tplVars);
})->setName('persons_edit');



$app->post('/persons/edit', function (Request $request, Response $response, $args) {
    $id_person = $request->getQueryParams()['id_person'];
    $formData = $request->getParsedBody();

    if (empty($formData['first_name']) || empty($formData['last_name']) || empty($formData['nickname'])) {
        $tplVars['message'] = 'Fill required fields';
    } else {
        if (!empty($formData['city']) || !empty($formData['street_name']) || !empty($formData['street_number']) || !empty($formData['zip'])) {
            $stmt = $this->db->query("SELECT id_location FROM person WHERE id_person = $id_person");
            $id_location = $stmt->fetch()['id_location'];
            if ($id_location != null) {
                try {
                    $stmt = $this->db->prepare("UPDATE location SET city = :city, street_name = :street_name,
                                                street_number = :street_number, zip = :zip, country = :country WHERE id_location = :id_location");
                    $stmt->bindValue(':city', empty($formData['city']) ? null : $formData['city']);
                    $stmt->bindValue(':street_name', empty($formData['street_name']) ? null : $formData['street_name']);
                    $stmt->bindValue(':street_number',empty($formData['street_number']) ? null : $formData['street_number']);
                    $stmt->bindValue(':zip', empty($formData['zip']) ? null : $formData['zip']);
                    $stmt->bindValue(':country', empty($formData['country']) ? null : $formData['country']);

                    $stmt->execute();
                } catch (PDOException $e){
                    $tplVars['message'] = 'Sorry, error location';
                    $this->logger->error($e->getMessage());
                }

            } else {
                $id_location = insert_location($this->db, $formData);
            }
        }
        try {
            $stmt = $this->db->prepare("UPDATE person SET nickname = :nickname, first_name = :first_name, last_name = :last_name, 
                                            birth_day = :birth_day, height = :height, gender = :gender, id_location = :id_location WHERE id_person = :id_person");
            $stmt->bindValue(':nickname', $formData['nickname']);
            $stmt->bindValue(':first_name', $formData['first_name']);
            $stmt->bindValue(':last_name', $formData['last_name']);
            $stmt->bindValue(':gender', empty($formData['gender']) ? null : $formData['gender'] );
            $stmt->bindValue(':height', empty($formData['height']) ? null : $formData['height']);
            $stmt->bindValue(':birth_day', empty($formData['birth_day']) ? null : $formData['birth_day']);
            $stmt->bindValue(':id_person', $id_person);
            $stmt->bindValue(':id_location', $id_location);
            $stmt->execute();
        } catch (PDOException $e){
            $tplVars['message'] = 'Sorry, error person info';
            $this->logger->error($e->getMessage());
        }
    }
    $tplVars['formData'] = $formData;

    return $this->view->render($response, 'person_edit.latte', $tplVars);

});


//_________________________________________________________________________________________
//Delete osoby
$app->post('/persons/delete', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    if (empty($params['id_person'])){
        exit('id person is missing');
    } else {
        try {
            $stmt = $this->db->prepare("DELETE FROM person WHERE id_person = :id_person");
            $stmt->bindValue(':id_person', $params['id_person']);
            $stmt->execute();
        } catch (PDOException $e) {
            $this->logger->info($e);
            exit('Sorry, error delete');
        }
    }
    return $response->withHeader('Location', $this->router->pathFor('persons'));

})->setName('person_delete');

//MEETINGS
//_________________________________________________________________________________________
//_________________________________________________________________________________________
//_________________________________________________________________________________________
//Vypsani vsech meetingu
$app->get('/meetings', function (Request $request, Response $response, $args) {
    $stmt['meetings'] = $this->db->query("SELECT * FROM meeting ORDER BY start")->fetchAll();


    return $this->view->render($response, 'meetings.latte', $stmt);
})->setName('meetings');


//_________________________________________________________________________________________
//info jednotliveho meetingu
$app->get('/meetings/info', function (Request $request, Response $response, $args) {
    //info o meetingu
    $id_meeting = $request->getQueryParams()['id_meeting'];
    $stmt = $this->db->prepare("SELECT * FROM meeting WHERE id_meeting = :id_meeting ORDER BY start");
    $stmt->bindValue(":id_meeting", empty($id_meeting) ? null : $id_meeting);
    $stmt->execute();
    $tplVars['meeting'] = $stmt->fetch();

    if (empty($tplVars['meeting'])) {
        exit('Meeting not found');
    }

    //info o lokaci meetingu
    $id_meeting = $request->getQueryParams()['id_meeting'];
    $stmt = $this->db->prepare("SELECT * FROM meeting LEFT JOIN location USING (id_location) WHERE id_meeting = :id_meeting");
    $stmt->bindValue(":id_meeting", empty($id_meeting) ? null : $id_meeting);
    $stmt->execute();
    $tplVars['meeting_location'] = $stmt->fetch();

    //info o osobach ucastnicich se meetingu
    $id_meeting = $request->getQueryParams()['id_meeting'];
    $stmt = $this->db->prepare("SELECT * FROM person LEFT JOIN person_meeting USING (id_person) WHERE id_meeting = :id_meeting");
    $stmt->bindValue(":id_meeting", empty($id_meeting) ? null : $id_meeting);
    $stmt->execute();
    $tplVars['meeting_persons'] = $stmt->fetchAll();



    return $this->view->render($response, 'meeting_info.latte', $tplVars);
})->setName('meeting_info');

//_________________________________________________________________________________________
//novy meeting
$app->get('/meetings/new', function (Request $request, Response $response, $args) {
    $tplVars['formData'] = [
        'start' => '',
        'description' => '',
        'duration' => '',
        'id_location' => null,
        'city' => '',
        'street_name' => '',
        'street_number' => '',
        'zip' => '',
    ];
    if (!empty($request->getQueryParams()['id_person'])) {
        $tplVars['id_person'] = $request->getQueryParams()['id_person'];
    }

    return $this->view->render($response, 'meeting_new.latte', $tplVars);
})->setName('meeting_new');

//_________________________________________________________________________________________
// Odeslani formulare pro pridani meetingu
$app->post('/meetings/new', function (Request $request, Response $response, $args) {
    $formData = $request->getParsedBody();
    $id_person = empty($request->getQueryParams()['id_person']) ? null : $request->getQueryParams()['id_person'];
    $formData['start'] = $formData['start_date'].' '.$formData['start_time'];

    if ($formData['start'] == ' ') {
        $tplVars['message'] = 'Fill required fields';
    } else {
        $id_location = insert_location($this->db, $formData);


        try {
            $stmt = $this->db->prepare("INSERT INTO meeting (start, description, duration, id_location)
                                        VALUES (:start, :description, :duration, :id_location)");
            $stmt->bindValue(':start', $formData['start']);
            $stmt->bindValue(':description', $formData['description']);
            $stmt->bindValue(':duration', empty($formData['duration']) ? null : $formData['duration']);
            $stmt->bindValue(':id_location', $id_location );
            $stmt->execute();
            if ($id_person != null) {
                $stmt = $this->db->prepare("INSERT INTO person_meeting (id_person, id_meeting) VALUES (:id_person, :id_meeting)");
                $stmt->bindValue(':id_person', $id_person);
                $stmt->bindValue(':id_meeting', $this->db->lastInsertId('meeting_id_meeting_seq'));
                $stmt->execute();
            }
        } catch (PDOException $e){
            $tplVars['message'] = 'Sorry, error';
            $this->logger->error($e->getMessage());
        }
    }
    $tplVars['formData'] = $formData;
    $date_time = $tplVars['formData']['start'];
    $tplVars['formData']['start_date'] = substr($date_time, 0, 10);
    $tplVars['formData']['start_time'] = substr($date_time, 11, 5);

    return $response->withHeader('Location', $this->router->pathFor('meetings'));
});



//_________________________________________________________________________________________
//formular pro edit meetingu
$app->get('/meetings/edit', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();

    if (empty($params['id_meeting'])) {
        exit('id_meeting is missing');
    } else {
        $stmt = $this->db->prepare("SELECT * FROM meeting LEFT JOIN location ON (meeting.id_location = location.id_location)
                                        WHERE id_meeting = :id_meeting");
        $stmt->bindValue(':id_meeting', $params['id_meeting']);
        $stmt->execute();
        $tplVars['formData'] = $stmt->fetch();

        if (empty($tplVars['formData'])) {
            exit('Meeting not found');
        }

        $date_time = $tplVars['formData']['start'];
        $tplVars['formData']['start_date'] = substr($date_time, 0, 10);
        $tplVars['formData']['start_time'] = substr($date_time, 11, 5);
    }

    return $this->view->render($response, 'meetings_edit.latte', $tplVars);
})->setName('meetings_edit');

//odeslani formulare pro edit meetingu
$app->post('/meetings/edit', function (Request $request, Response $response, $args) {
    $id_meeting = $request->getQueryParams()['id_meeting'];
    $formData = $request->getParsedBody();
    $formData['start'] = $formData['start_date'].' '.$formData['start_time'];

    if (empty($formData['start'])) {
        $tplVars['message'] = 'Fill required fields';
    } else {
        if (empty($formData['city']) || empty($formData['street_name']) || empty($formData['street_number']) || empty($formData['zip'])
                || empty($formData['description']) || empty($formData['duration'])) {
            $stmt = $this->db->prepare("SELECT id_location FROM meeting WHERE id_meeting = :id_meeting");
            $stmt->bindValue(':id_meeting', $id_meeting);
            $stmt->execute();
            $id_location = $stmt->fetch()['id_location'];
            if ($id_location != null) {
                try {
                    $stmt = $this->db->prepare("UPDATE location SET city = :city, street_name = :street_name,
                                                street_number = :street_number, zip = :zip, country = :country WHERE id_location = :id_location");
                    $stmt->bindValue(':city', empty($formData['city']) ? null : $formData['city']);
                    $stmt->bindValue(':street_name', empty($formData['street_name']) ? null : $formData['street_name']);
                    $stmt->bindValue(':street_number',empty($formData['street_number']) ? null : $formData['street_number']);
                    $stmt->bindValue(':zip', empty($formData['zip']) ? null : $formData['zip']);
                    $stmt->bindValue(':country', empty($formData['country']) ? null : $formData['country']);
                    $stmt->bindValue(':id_location', $id_location);

                    $stmt->execute();
                } catch (PDOException $e){
                    $tplVars['message'] = 'Sorry, error location';
                    $this->logger->error($e->getMessage());
                }

            } else {
                insert_location($this->db, $formData);
            }
        }
        try {
            $stmt = $this->db->prepare("UPDATE meeting SET start = :start, description = :description, duration = :duration 
                                            WHERE id_meeting = :id_meeting");
            $stmt->bindValue(':start', $formData['start']);
            $stmt->bindValue(':duration', empty($formData['duration']) ? null :$formData['duration'] );
            $stmt->bindValue(':description', empty($formData['description']) ? null :$formData['description']);
            $stmt->bindValue(':id_meeting', $id_meeting);

            $stmt->execute();
        } catch (PDOException $e){
            $tplVars['message'] = 'Sorry, error meeting info';
            $this->logger->error($e->getMessage());
        }
    }
    $tplVars['formData'] = $formData;
    return $this->view->render($response, 'meetings_edit.latte', $tplVars);

});

//_________________________________________________________________________________________
//delete osoby z meetingu
$app->post('/meetings/info/delete_person', function (Request $request, Response $response, $args){
    $params = $request->getQueryParams();
    $id_meeting = $params['id_meeting'];
    $id_person = $params['id_person'];
    if (empty($params['id_meeting']) || empty($params['id_person'])) {
        exit('id meeting or person is missing');
    } else {
        try {
            $stmt = $this->db->prepare("DELETE FROM person_meeting WHERE id_person = :id_person AND id_meeting = :id_meeting");
            $stmt->bindValue(':id_person', $params['id_person']);
            $stmt->bindValue(':id_meeting', $params['id_meeting']);
            $stmt->execute();
        } catch (PDOException $e) {
            $this->logger->info($e);
            exit('Sorry, error delete person from meeting');
        }
    }

    $params_out['id_meeting'] = $id_meeting;
    return $response->withRedirect( $this->router->pathFor('meeting_info', [], $params_out));

})->setName('delete_person_from_meeting');


//_________________________________________________________________________________________
//delete meetingu
$app->post('/meetings/delete', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    if (empty($params['id_meeting'])){
        exit('id meeting is missing');
    } else {
        try {
            $stmt = $this->db->prepare("DELETE FROM meeting WHERE id_meeting = :id_meeting");
            $stmt->bindValue(':id_meeting', $params['id_meeting']);
            $stmt->execute();
        } catch (PDOException $e) {
            $this->logger->info($e);
            exit('Sorry, error delete');
        }
    }
    return $response->withHeader('Location', $this->router->pathFor('meetings'));

})->setName('meeting_delete');

//________________________________________________________________________________________
//pridani osoby do meetingu
$app->get('/meetings/info/person_add', function (Request $request, Response $response, $args) {
    $id_meeting = $request->getQueryParams()['id_meeting'];
    $tplVars['formData'] = [
        'id_person' => '',
    ];
    $stmt = $this->db->prepare("SELECT p.id_person, first_name, last_name FROM person p
                                LEFT JOIN person_meeting pm ON p.id_person = pm.id_person
                                WHERE pm.id_meeting != :id_meeting 
                                ORDER BY first_name");
    $stmt->bindValue(':id_meeting', $id_meeting);
    $stmt->execute();
    $tplVars['persons'] = $stmt->fetchAll();

    return $this->view->render($response, 'meeting_person_add.latte', $tplVars);
})->setName('person_add');

$app->post('/meetings/info/person_add', function (Request $request, Response $response, $args) {
    $formData = $request->getParsedBody();
    $id_meeting = $request->getQueryParams()['id_meeting'];

    if (empty($formData['id_person'])) {
        $tplVars['message'] = 'Fill required fields';
    } else {
        try {
            $stmt = $this->db->prepare("INSERT INTO person_meeting (id_person, id_meeting)
                                        VALUES (:id_person, :id_meeting)");
            $stmt->bindValue(':id_person', $formData['id_person']);
            $stmt->bindValue(':id_meeting', $id_meeting);

            $stmt->execute();
        } catch (PDOException $e){
            $tplVars['message'] = 'Sorry, error';
            $this->logger->error($e->getMessage());
        }
    }
    $tplVars['formData'] = $formData;
    $params_out['id_meeting'] = $id_meeting;

    return $response->withRedirect($this->router->pathFor('meeting_info', [], $params_out));
});


//CONTACTS
//_________________________________________________________________________________________
//_________________________________________________________________________________________
//_________________________________________________________________________________________
//zobrazeni formulare pro edit kontaktu
$app->get('/persons/edit/contact_edit', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    if (empty($params['id_contact'])) {
        exit('id contact is missing');
    } else {
        $stmt = $this->db->prepare("SELECT * FROM contact LEFT JOIN contact_type USING (id_contact_type)
                                    WHERE id_contact = :id_contact");
        $stmt->bindValue(':id_contact', $params['id_contact']);
        $stmt->execute();
        $tplVars['formData'] = $stmt->fetch();
        if (empty($tplVars['formData'])) {
            exit('Contact not found');
        }
    }
    $stmt = $this->db->query("SELECT * FROM contact_type");
    $tplVars['contact_type'] = $stmt->fetchAll();

    return $this->view->render($response, 'contact_edit.latte', $tplVars);
})->setName('contact_edit');


//_________________________________________________________________________________________
//odeslani formulare kontaktu do databaze
$app->post('/persons/edit/contact_edit', function (Request $request, Response $response, $args) {
    $id_contact = $request->getQueryParams()['id_contact'];
    $formData = $request->getParsedBody();

    if (empty($formData['contact']) || empty($formData['id_contact_type']) ) {
        $tplVars['message'] = 'Fill required fields';
    } else {
        try {
            $stmt = $this->db->prepare("UPDATE contact SET contact = :contact WHERE id_contact = :id_contact");
            $stmt->bindValue(':contact', $formData['contact']);
            $stmt->bindValue(':id_contact', $id_contact);
            $stmt->execute();

        } catch (PDOException $e){
            $tplVars['message'] = 'Sorry, error contact form';
            $this->logger->error($e->getMessage());
        }
    }
    $tplVars['formData'] = $formData;
    $stmt = $this->db->query("SELECT * FROM contact_type");
    $tplVars['contact_type'] = $stmt->fetchAll();

    return $this->view->render($response, 'contact_edit.latte', $tplVars);
});
//_________________________________________________________________________________________
//delete kontaktu
$app->post('/persons/info/contact_delete', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    $id_contact = $params['id_contact'];
    $id_person = $params['id_person'];


    if (empty($params['id_contact'])){
        exit('id contact is missing');
    } else {
        try {
            $stmt = $this->db->prepare("DELETE FROM contact WHERE id_contact = :id_contact");
            $stmt->bindValue(':id_contact', $id_contact);
            $stmt->execute();
        } catch (PDOException $e) {
            $this->logger->info($e);
            exit('Sorry, error delete');
        }
    }
    $params_out['id_person'] = $id_person;
    return $response->withRedirect( $this->router->pathFor('persons_info', [], $params_out));

})->setName('contact_delete');

//________________________________________________________________________________________
//pridani kontaktu
$app->get('/persons/info/contact_new', function (Request $request, Response $response, $args) {
    $tplVars['formData'] = [
        'contact' => '',
        'id_contact_type' => '',
    ];
    $stmt = $this->db->query("SELECT * FROM contact_type");
    $tplVars['contact_type'] = $stmt->fetchAll();
    return $this->view->render($response, 'contact_new.latte', $tplVars);
})->setName('contact_new');

$app->post('/persons/info/contact_new', function (Request $request, Response $response, $args) {
    $formData = $request->getParsedBody();
    $id_person = $request->getQueryParams()['id_person'];

    $stmt = $this->db->query("SELECT * FROM contact_type");
    $tplVars['contact_type'] = $stmt->fetchAll();

    if (empty($formData['contact']) || empty($formData['id_contact_type']) ) {
        $tplVars['message'] = 'Fill required fields';
    } else {
        try {
            $stmt = $this->db->prepare("INSERT INTO contact (id_contact_type, contact, id_person)
                                        VALUES (:id_contact_type, :contact, :id_person)");
            $stmt->bindValue(':contact', $formData['contact']);
            $stmt->bindValue(':id_contact_type', $formData['id_contact_type']);
            $stmt->bindValue('id_person', $id_person);
            $stmt->execute();
        } catch (PDOException $e){
            $tplVars['message'] = 'Sorry, error';
            $this->logger->error($e->getMessage());
        }
    }
    $tplVars['formData'] = $formData;
    $params_out['id_person'] = $id_person;

    return $response->withRedirect($this->router->pathFor('persons_info', [], $params_out));
});




//RELATIONS
//_________________________________________________________________________________________
//_________________________________________________________________________________________
//_________________________________________________________________________________________
//zobrazeni formulare pro edit vztahu
$app->get('/persons/edit/relation_edit', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    if (empty($params['id_relation'])) {
        exit('id relation is missing');
    } else {
        $stmt = $this->db->prepare("SELECT * FROM relation LEFT JOIN relation_type USING (id_relation_type)
                                    WHERE id_relation = :id_relation");
        $stmt->bindValue(':id_relation', $params['id_relation']);
        $stmt->execute();
        $tplVars['formData'] = $stmt->fetch();

        $tplVars['persons'] = $this->db->query("SELECT * FROM person ORDER BY first_name")->fetchAll();


        if (empty($tplVars['formData'])) {
            exit('relation not found');
        }
    }
    $stmt = $this->db->query("SELECT * FROM relation_type");
    $tplVars['relation_type'] = $stmt->fetchAll();

    return $this->view->render($response, 'relation_edit.latte', $tplVars);
})->setName('relation_edit');


//_________________________________________________________________________________________
//odeslani formulare vztahu do databaze
$app->post('/persons/edit/relation_edit', function (Request $request, Response $response, $args) {
    $id_relation = $request->getQueryParams()['id_relation'];
    $formData = $request->getParsedBody();
    $stmt = $this->db->query("SELECT * FROM relation_type");
    $tplVars['relation_type'] = $stmt->fetchAll();

    if (empty($formData['id_person2']) || empty($formData['id_relation_type']) ) {
        $tplVars['message'] = 'Fill required fields';
    } else {
        try {
            $stmt = $this->db->prepare("UPDATE relation SET  id_person2 = :id_person2, id_relation_type = :id_relation_type
                                        WHERE id_relation = :id_relation");
            $stmt->bindValue(':id_person2', $formData['id_person2']);
            $stmt->bindValue(':id_relation_type', $formData['id_relation_type']);
            $stmt->bindValue(':id_relation', $id_relation);
            $stmt->execute();

        } catch (PDOException $e){
            $tplVars['message'] = 'Sorry, error relation';
            $this->logger->error($e->getMessage());
        }
    }
    $tplVars['formData'] = $formData;
    return $this->view->render($response, 'relation_edit.latte', $tplVars);
});
//_________________________________________________________________________________________
//delete vztahu
$app->post('/persons/info/relation_delete', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    $id_relation = $params['id_relation'];
    $id_person = $params['id_person'];

    if (empty($params['id_relation'])){
        exit('id relation is missing');
    } else {
        try {
            $stmt = $this->db->prepare("DELETE FROM relation WHERE id_relation = :id_relation");
            $stmt->bindValue(':id_relation', $id_relation);
            $stmt->execute();
        } catch (PDOException $e) {
            $this->logger->info($e);
            exit('Sorry, error delete');
        }
    }
    $params_out['id_person'] = $id_person;
    return $response->withRedirect( $this->router->pathFor('persons_info', [], $params_out));

})->setName('relation_delete');

//________________________________________________________________________________________
//pridani kontaktu
$app->get('/persons/info/relation_new', function (Request $request, Response $response, $args) {
    $tplVars['formData'] = [
        'id_person2' => '',
        'id_relation_type' => '',
        'description' => '',
    ];
    $stmt = $this->db->query("SELECT id_person, first_name, last_name FROM person ORDER BY first_name");
    $tplVars['persons'] = $stmt->fetchAll();
    $stmt = $this->db->query("SELECT * FROM relation_type");
    $tplVars['relation_type'] = $stmt->fetchAll();
    return $this->view->render($response, 'relation_new.latte', $tplVars);
})->setName('relation_new');

$app->post('/persons/info/relation_new', function (Request $request, Response $response, $args) {
    $formData = $request->getParsedBody();
    $id_person = $request->getQueryParams()['id_person'];
    $stmt = $this->db->query("SELECT * FROM relation_type");
    $tplVars['relation_type'] = $stmt->fetchAll();

    if (empty($formData['id_relation_type']) || empty($formData['id_person2']) ) {
        $tplVars['message'] = 'Fill required fields';
    } else {
        try {
            $stmt = $this->db->prepare("INSERT INTO relation (id_person1, id_person2, id_relation_type, description)
                                        VALUES (:id_person1, :id_person2, :id_relation_type, :description)");
            $stmt->bindValue(':id_person2', $formData['id_person2']);
            $stmt->bindValue(':id_relation_type', $formData['id_relation_type']);
            $stmt->bindValue('description', $formData['description']);
            $stmt->bindValue(':id_person1', $id_person);
            $stmt->execute();
        } catch (PDOException $e){
            $tplVars['message'] = 'Sorry, error';
            $this->logger->error($e->getMessage());
        }
    }

    $tplVars['formData'] = $formData;
    $params_out['id_person'] = $id_person;

    return $response->withRedirect($this->router->pathFor('persons_info', [], $params_out));
});










